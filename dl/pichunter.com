#!/usr/bin/python

import traceback
from leech.loader import *

config = {
    'start':  1063700,
    'stop' : 10000000,
    'url'  : 'http://www.pichunter.com/gallery/'
}


for i in range(config['start'], config['stop']):
    printPercent(i, config)

    try:
        getGallery(i, config)
    except KeyboardInterrupt:
        quit()
    except Exception as e:
        traceback.print_exc()
