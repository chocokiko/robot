# Todo: FileDoc


class Config(object):
    # Todo Documentation
    def __init__(self):
        self.Source = {
            'galleries.grooby.com': {
                'ImageSelector':     None,
                'ImageAttribute':    None,
                'MetaDataSelector':  None,
                'MetaDataAttribute': None
            },
            'join.lbgirlfriends.com': {
                'ImageSelector':     'div.white_content img', 
                'ImageAttribute':    'src',
                'MetaDataSelector':  None,
                'MetaDataAttribute': None
            },
            'join.ladyboysfuckedbareback.com': {
                'ImageSelector':     'a.swipebox.fancybox', 
                'ImageAttribute':    'href',
                'MetaDataSelector':  None,
                'MetaDataAttribute': None
            },
            '__Default__': {
                'ImageSelector':     'a:has(img)',
                'ImageAttribute':    'href',
                'MetaDataSelector':  None,
                'MetaDataAttribute': None
            },
        }
        
        self.BasePath = './unsorted/kathoeypics.com'


