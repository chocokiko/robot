#!/usr/bin/python

from leech.Page import Page


class ListPage(Page):
    """
    Handle a listing page
     - hasSubpages
       -> followSubPageNav
     - hasCategories
       -> overrides hasSubpages
       -> get and iterate categories
    """

    def __init__(self, url):
        # Todo: ApiDoc
        super(ListPage(), self).__init__(
            url
        )
    
