"""
    @Todo: File ApiDoc
"""
# Todo: File ApiDoc

import requests
import traceback
import os


class Picture(object):
    """
    Handle one picture
        - create filename
        - fetch file from internet
        - save to correct path
    """

    def __init__(self, url, metadata, path, dry_run=False):
        """

        :param metadata:
        :param path:
        :return:
        """
        # Todo: Docstring
        self.url = url
        self.data = metadata
        self.basepath = path
        self.dry_run = dry_run

        self.prefix = ""
        self.subpath = ""
        self.count = 0
        self.__picresponse = None

    def __str__(self):
        # Todo: ApiDoc
        return str(self.url)

    def load(self):
        """
        Fetch image from web by requests & write it to disk

        :param dry_run: bool don't load pictures
        :return: None
        """
        try:
            # Pictures need to be streamed
            if self.dry_run is True:
                print 'picture.py[47]:', self.url, self.get_full_path()

            else:
                # Todo: check if file already exists?
                self.__picresponse = requests.get(self.url, stream=True)
                if self.__picresponse.status_code == 200:

                    # make sure download dir exists
                    if not os.path.exists(os.path.dirname(self.get_full_path())):
                        os.makedirs(os.path.dirname(self.get_full_path()))

                    # write file
                    with open(self.get_full_path(), 'wb') as f:
                        for chunk in self.__picresponse.iter_content(1024):
                            f.write(chunk)
                    return True
                else:
                    # Todo: Response error handling
                    pass

        except KeyboardInterrupt:
            # This step takes the most time so make sure we can drop out here
            quit()
        except Exception as e:
            # Todo: Throw a custom exception here?
            traceback.print_exc()
            return None

    def get_file_name(self):
        """
        get picture file name

        :return: string filename
        """
        return '.'.join([
            self.get_prefix(),
            self.get_count(),
            self.get_extension()
        ])

    def get_prefix(self):
        """

        :return: string file prefix
        """
        # Todo Implement (generate from Metadata)
        return ''

    def get_count(self):
        """

        :return: string file count
        """
        # Todo Implement (generate from Metadata)
        return ''

    def get_extension(self):
        """

        :return: string file count
        """
        # Todo Implement (generate from Metadata)
        return ''

    def get_full_path(self):
        # Todo: Docstring
        return '/'.join([
            self.get_base_path(),
            self.get_sub_path(),
            self.get_file_name(),
        ])

    def get_base_path(self):
        # Todo: Docstring
        return os.path.abspath(self.basepath)

    def get_sub_path(self):
        # Todo: Docstring
        return self.subpath
