"""
File ApiDoc
"""

import requests
import os
import slugify
import sys
import traceback
from pyquery import PyQuery as pq


def getHtml(url):
    try:
        r = requests.get(url)
        return pq(r.text)
    except KeyboardInterrupt:
        quit()
    except:
        return None


def getPicture(url, imgpath):
    r = requests.get(url, stream=True)
    if r.status_code == 200:

        # make sure download dir exists
        if not os.path.exists(os.path.dirname(imgpath)):
            os.makedirs(os.path.dirname(imgpath))

        with open(imgpath, 'wb') as f:
            for chunk in r.iter_content(1024):
                f.write(chunk)


def getPicsInGallery(gallery, prefix):
    for pic in gallery('figure a.item'):
        link = pq(pic).attr.href
        count = link.split('_')[1]
        path = './pic/' + prefix + '.' + count + '.jpg'
        sys.stdout.write(count + ' ')
        getPicture(link, path)

    print


def getGallery(index, config):
    gallery = getHtml(config['url'] + str(index))
    if gallery != None:
        headline = gallery('h1').text().strip()
        slug = slugify.slugify_filename(headline)

        if headline != "Woops ! GALLERY":
            tags = gallery('ul.tagCloud li a').text().strip().split(' ')
            prefix = str(index) + '|' + slug + '|' + '_'.join(tags)
            print prefix

            getPicsInGallery(gallery, prefix)


def printPercent(i, config):
    if i % 100 == 0:
        print '--->', i, '  ', (float(i) / float(config['stop'])) * 100


