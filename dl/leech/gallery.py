"""
    @Todo: File ApiDoc
"""
# Todo: File ApiDoc

import os

from pyquery import PyQuery

from leech.picture import Picture
from leech.page import Page

class Gallery(Page):
    """
        Handle a single gallery
        - request gallery page
        - collect gallery metadata
        - collect image links and form Picture objects
        - make images iteratable for download
    """

    def __init__(self, url, dry_run=False):
        # Todo ApiDoc
        super(Gallery, self).__init__(
            url
        )

        self.dry_run = dry_run

        self.metadata['galleryIndex'] = 0
        self.metadata['galleryPage'] = self.get_html()

        self.pictures = self.get_pictures()

    def __iter__(self):
        """
        initialize iteration
        :return: self as Iterator
        """
        return self

    def next(self):
        """
        Return next picture for iteration
        :return: next Picture object
        """
        if self.metadata['galleryIndex'] >= len(self.pictures):
            raise StopIteration
        else:
            self.metadata['galleryIndex'] += 1
            return self.pictures[self.metadata['galleryIndex'] - 1]

    def get_image_config(self, item):
        """
        Return site specific image config or default

        :return: string
        """
        for key in self.config.Source.keys():
            if key in self.metadata['url']:
                return self.config.Source[key][item]

        return self.config.Source['__Default__'][item]

    def get_pictures(self):
        # Todo: apidoc
        result = []
        selector = self.get_image_config('ImageSelector')
        attribute = self.get_image_config('ImageAttribute')
        page = self.metadata['galleryPage']

        for picture in page(selector):
            picture = PyQuery(picture)
            path = os.path.split(self.response.url)[0]

            if ".jpg" in picture.attr(attribute):
                pic = Picture(
                    str(path + '/' + picture.attr(attribute)),
                    self.metadata,
                    self.config.BasePath,
                    self.dry_run
                )
                result.append(pic)

        return result