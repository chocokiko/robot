"""
Todo: File ApiDoc
"""

import requests
import traceback

from pyquery import PyQuery

from leech.config import Config


class Page(object):
    """
    Base class for thumbnail page handlers
    
    Todo: Class Apidoc
    """
    
    def __init__(self, url):
        """
        Todo: ApiDoc
        """
        self.config = Config()
        self.response = None
        
        self.metadata = {}
        self.metadata['url'] = url

    
    def get_html(self):
        '''
        get HTML by requests api & wrap it in PyQuery

        :return: PyQuery set | None
        '''
        try:
            self.response = requests.get(self.metadata['url'])
            return PyQuery(self.response.text)
        except KeyboardInterrupt:
            # This step takes the most time so make sure we can drop out here
            quit()
        except:
            traceback.print_exc
            return None