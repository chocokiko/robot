#!/usr/bin/python

import os
from leech.loader import getPicture


config = {
    'url':      'http://www.ladyboysheaven.com/aff/fhg/ladyboy-%d/images/%02d.jpg',
    'imgpath':  'ladyboysheaven.com/ladyboy-%d_%02d.jpg',
    'start':        0,
    'stop':      1000
}


for i in range(config['start'], config['stop']):
    print config['imgpath'] % (i, 1)
    for j in range(40):
        getPicture(
            config['url'] % (i, j),
            config['imgpath'] % (i, j)
        )