#!/usr/bin/python

import pprint

from pyquery import PyQuery

from leech.gallery import Gallery
from leech.page import Page


class KathoeyPicsComLoader(Page):
    """
        load image galleries by thumbnail page
    """
    def __init__(self):
        # Todo: ApiDoc
        super(KathoeyPicsComLoader, self).__init__(
            'http://www.kathoeypics.com'
        )

        self.gallerySelector = 'a.pi'
        self.dry_run = True

    def getGalleryPages(self):
        '''

        :return: List of gallery pages
        '''
        result = [
            self.get_html()
        ]
        return result

    def getGalleryLinks(self, page):
        '''

        :param page: PyQuery wrapped html page with galleries
        :return: list of gallery links
        '''
        result = []
        for link in page(self.gallerySelector):
            result.append(PyQuery(link))
        return result

    def run(self):
        """
        run the image loader

        :return:
        """
        for page in self.getGalleryPages():
            i = 1
            for link in self.getGalleryLinks(page):
                print 'kathoeypics.com[53]:', link.attr.title, link.attr.href, PyQuery(link.html()).attr.src

                gallery = Gallery(link.attr.href, self.dry_run)
                gallery.metadata['galleryTitle'] = link.attr.title
                gallery.metadata['galleryIndex'] = i

                for picture in gallery:
                    picture.load()

                pprint.pprint(gallery.metadata)

                if i > 4:
                    quit()

                i += 1

        quit()


if __name__ == "__main__":
    kLoader = KathoeyPicsComLoader()
    kLoader.run()
