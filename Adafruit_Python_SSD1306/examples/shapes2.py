# Copyright (c) 2014 Adafruit Industries
# Author: Tony DiCola
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
# import time

# import Adafruit_GPIO.SPI as SPI
import Adafruit_SSD1306

import Image
import ImageDraw
import ImageFont


# Raspberry Pi pin configuration:
RST = 24

# 128x32 display with hardware I2C:
# disp = Adafruit_SSD1306.SSD1306_128_32(rst=RST)

# 128x64 display with hardware I2C:
disp = Adafruit_SSD1306.SSD1306_128_64(rst=RST)

# Note you can change the I2C address by passing an i2c_address parameter like:
# disp = Adafruit_SSD1306.SSD1306_128_64(rst=RST, i2c_address=0x3C)

# Alternatively you can specify an explicit I2C bus number, for example
# with the 128x32 display you would use:
# disp = Adafruit_SSD1306.SSD1306_128_32(rst=RST, i2c_bus=2)

# Initialize library.
disp.begin()

# Clear display.
disp.clear()
disp.display()

# Create blank image for drawing.
# Make sure to create image with mode '1' for 1-bit color.
width = disp.width
height = disp.height
image = Image.new('1', (width, height))

# Get drawing object to draw on image.
draw = ImageDraw.Draw(image)

# Draw a black filled box to clear the image.
draw.rectangle((0, 0, width, height), outline=0, fill=0)

padding = 2
top = padding

x = padding

# Load default font.
# font = ImageFont.load_default()
# draw.text((x, top + 50), 'Hello 1234567890', font=font, fill=255)

# Alternatively load a TTF font.  Make sure the .ttf font file is in the same
# directory as the python script!
# Some other nice fonts to try: http://www.dafont.com/bitmap.php
# font = ImageFont.truetype('Minecraftia-Regular.ttf', 8)
# draw.text((x, top), 'Hello 1234567890', font=font, fill=255)

font = ImageFont.truetype('Rygarde.ttf', 8)
# draw.text((x, top + 10), 'Hello 1234567890-.!', font=font, fill=255)

# font = ImageFont.truetype('zxspectr.ttf', 8)
# draw.text((x, top + 18), 'Hello 1234567890.-!', font=font, fill=255)

# font = ImageFont.truetype('apple2.ttf', 8)
# draw.text((x, top + 26), 'Hello 1234567890', font=font, fill=255)

t1='Sed ut perspiciatis unde omnis '
t2='iste natus error sit voluptatem' 
t3='accusantium doloremque laudantium,'
t4='totam rem aperiam, eaque ipsa'
t5='quae ab illo inventore veritatis'
t6='et quasi architecto beatae vitae'
t7='dicta sunt explicabo. Nemo enim'
t8='ipsam voluptatem quia voluptas'
t9='sit aspernatur aut odit aut'
t0='fugit, sed quia consequuntur'
ta='magni dolores eos qui ratione'
tb='voluptatem sequi nesciunt. '

draw.text((x, top), t1, font=font, fill=255)
draw.text((x, top + 8), t2, font=font, fill=255)
draw.text((x, top + 16), t3, font=font, fill=255)
draw.text((x, top + 24), t4, font=font, fill=255)
draw.text((x, top + 32), t5, font=font, fill=255)
draw.text((x, top + 40), t6, font=font, fill=255)
draw.text((x, top + 48), t7, font=font, fill=255)
draw.text((x, top + 56), t8, font=font, fill=255)
draw.text((x, top + 64), t9, font=font, fill=255)
draw.text((x, top + 72), t0, font=font, fill=255)

# Display image.
disp.image(image)
disp.display()
