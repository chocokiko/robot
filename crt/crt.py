'''
    crt.py: Port of GNU Pascal's Unit CRT
'''
__author__ = "Martin Toennishoff"
__copyright__ = "Copyright 2015"
__credits__ = ["Martin Toennishoff"]
__license__ = "GPL"
__version__ = "1.0.1"
__maintainer__ = "Martin Toennishoff"
__email__ = None
__status__ = "Development"


import sys


# Constants
# CRT modes
BW40 = 0                # 40x25 Black/White
CO40 = 1                # 40x25 Color
BW80 = 2                # 80x25 Black/White
CO80 = 3                # 80x25 Color
Mono = 7                # 80x25 Black/White
Font8x8 = 256           # Add-in for 80x43 or 80x50 mode

# Foreground and background color constants
Black = 0
Blue = 1
Green = 2
Cyan = 3
Red = 4
Magenta = 5
Brown = 6
LightGray = 7

# Foreground color constants
DarkGray = 8
LightBlue = 9
LightGreen = 10
LightCyan = 11
LightRed = 12
LightMagenta = 13
Yellow = 14
White = 15

# Add-in for blinking
Blink = 128


class Crt(object):
    """
        Port of GNU Pascal Unit Crt
    """
    def __init__(self, arg):
        super(Crt, self).__init__()

        # Variables
        # { If False (default: True), catch interrupt signals (SIGINT;
        #   Ctrl-C), and other flow control characters as well as SIGTERM,
        #   SIGHUP and perhaps other signals }
        ''' @var boolean '''
        self.CheckBreak = True

        # { If True (default : False), replace Ctrl-Z by #0 in input }
        ''' @var boolean '''
        self.CheckEOF = False

        # { Ignored -- meaningless here }
        ''' @var boolean '''
        self.DirectVideo = True

        # { Ignored -- curses or the terminal driver will take care of that
        #   when necessary }
        ''' @var boolean '''
        self.CheckSnow = False

        # { Current (sic!) text mode }
        ''' @var int '''
        self.LastMode = 3

        # { Current text attribute }
        ''' @var int '''
        self.TextAttr = 7

        # { Window upper left coordinates. *Obsolete*! Please see WindowMin
        #   below. }
        ''' @var int '''
        self.WindMin = sys.maxint

        # { Window lower right coordinates. *Obsolete*! Please see WindowMax
        #   below. }
        self.WindMax = sys.maxint

        '''
            If CRT is initialized automatically, not via CRTInit, and
            CRTAutoInitProc is not nil, it will be called before actually
            initializing CRT.
            @var function/None
        '''
        self.CRTAutoInitProc = None

    '''
        @param file f
        @return None
    '''
    def AssignCRT(self, f):
        pass

    '''
        Not effective on all platforms, see above. See also SetScreenSize
        and SetMonochrome.
        @param int mode
    '''
    def TextMode(self, Mode):
        pass

    '''
        @param int x1
        @param int y1
        @param int x2
        @param int y2
        @return None
    '''
    def Window(self, x1, y1, x2, y2):
        pass

    '''
        @param int x
        @param int y
        @return None
    '''
    def GotoXY(self, x, y):
        pass

    '''
        @return int
    '''
    def WhereX(self):
        pass

    '''
        @return int
    '''
    def WhereY(self):
        pass

    '''
        @return None
    '''
    def ClrScr(self):
        pass

    '''
        @return None
    '''
    def ClrEOL(self):
        pass

    '''
        @return None
    '''
    def InsLine(self):
        pass

    '''
        @return None
    '''
    def DelLine(self):
        pass

    '''
        @param int
        @return None
    '''
    def TextColor(self, Color):
        pass

    '''
        @param int
        @return None
    '''
    def TextBackground(self, Color):
        pass

    '''
        @return None
    '''
    def LowVideo(self):
        pass

    '''
        @return None
    '''
    def HighVideo(self):
        pass

    '''
        @return None
    '''
    def NormVideo(self):
        pass


    '''
        =================== Extensions over BP's CRT ===================

        Initializes the CRT unit. Should be called before using any of
        CRT's routines.

        Note: For BP compatibility, CRT is initizalized automatically when
        (almost) any of its routines are used for the first time. In this
        case, some defaults are set to match BP more closely. In
        particular, the PC charset (see SetPCCharSet) is enabled then
        (disabled otherwise), and the update level (see SetCRTUpdate) is
        set to UpdateRegularly (UpdateWaitInput otherwise). This feature
        is meant for BP compatibility *only*. Don't rely on it when
        writing a new program. Use CRTInit then, and set the defaults to
        the values you want explicitly.

        SetCRTUpdate is one of those few routines which will not cause CRT
        to be initialized immediately, and a value set with it will
        survive both automatic and explicit initialization, so you can use
        it to set the update level without caring which way CRT will be
        initialized. (This does not apply to SetPCCharSet. Since it works
        on a per-panel basis, it has to initialize CRT first, so there is
        a panel to start with.)

        If you terminate the program before calling CRTInit or any routine
        that causes automatic initialization, curses will never be
        initialized, so e.g., the screen won't be cleared. This can be
        useful, e.g., to check the command line arguments (or anything
        else) and if there's a problem, write an error and abort. Just be
        sure to write the error to StdErr, not Output (because Output will
        be assigned to CRT, and therefore writing to Output will cause CRT
        to be initialized, and because errors belong to StdErr, anyway),
        and to call RestoreTerminal (True) before (just to be sure, in
        case some code -- perhaps added later, or hidden in the
        initialization of some unit -- does initialize CRT).

        @return None
    '''
    def CRTInit(self):
        pass

    '''
        Changes the input and output file and the terminal description CRT
        uses. Only effective with ncurses, and only if called before CRT
        is initialized (automatically or explicitly; see the comments for
        CRTInit). If TerminalType is nil, the default will be used. If
        InputFile and/or OutputFile are Null, they remain unchanged.
        @param string
        @param file
        @param file
        @return None
    '''
    def CRTSetTerminal(self, TerminalType, InputFile, OutputFile):
        pass

    '''
        If called with an argument True, it causes CRT to save the
        previous screen contents if possible (see the comments at the
        beginning of the unit), and restore them when calling
        RestoreTerminal (True). After RestoreTerminal (False), they're
        saved again, and at the end of the program, they're restored. If
        called with an argument False, it will prohibit this behaviour.
        The default, if this def is not called, depends on the
        terminal (generally it is active on most xterms and similar and
        not active on most other terminals).

        This def should be called before initializing CRT (using
        CRTInit or automatically), otherwise the previous screen contents
        may already have been overwritten. It has no effect under XCurses,
        because the program uses its own window, anyway.
        @param boolean
    '''
    def CRTSavePreviousScreen(self, On):
        pass

    '''
        Returns True if CRTSavePreviousScreen was called with argument
        True and the functionality is really available. Note that the
        result is not reliable until CRT is initialized, while
        CRTSavePreviousScreen should be called before CRT is initialized.
        That's why they are two separate routines.
        @return boolean
    '''
    def CRTSavePreviousScreenWorks(self):
        pass

    '''
        Aborts with a runtime error saying that CRT was not initialized.
        If you set CRTAutoInitProc to this procedure, you can effectively
        disable CRT's automatic initialization.
        @return None
    '''
    def CRTNotInitialized(self):
        pass

    '''
        Set terminal to shell or curses mode. An internal procedure
        registered by CRT via RegisterRestoreTerminal does this as well,
        so CRTSetCursesMode has to be called only in unusual situations,
        e.g. after executing a process that changes terminal modes, but
        does not restore them (e.g. because it crashed or was killed), and
        the process was not executed with the Execute routine, and
        RestoreTerminal was not called otherwise. If you set it to False
        temporarily, be sure to set it back to True before doing any
        further CRT operations, otherwise the result may be strange.
        @param boolean
        @return None
    '''
    def CRTSetCursesMode(self, On):
        pass

    '''
        Do the same as RestoreTerminal (True), but also clear the screen
        after restoring the terminal (except for XCurses, because the
        program uses its own window, anyway). Does not restore and save
        again the previous screen contents if CRTSavePreviousScreen was
        called.
        @return None
    '''
    def RestoreTerminalClearCRT(self):
        pass

    '''
    { Keyboard and character graphics constants -- BP compatible! =:-}
    {$i crt.inc}

    var
        { Tells whether the XCurses version of CRT is used }
        XCRT: Boolean = {$ifdef XCURSES} True {$else} False {$endif};
        attribute (name = 'crt_XCRT');

        { If True (default: False), the Beep def and writing #7 do a
        Flash instead }
        VisualBell: Boolean = False; attribute (name = 'crt_VisualBell');

        { Cursor shape codes. Only to be used in very special cases. }
        CursorShapeHidden: CInteger = 0; attribute (name
        = 'crt_CursorShapeHidden');
        CursorShapeNormal: CInteger = 1; attribute (name
        = 'crt_CursorShapeNormal');
        CursorShapeFull:   CInteger = 2; attribute (name
        = 'crt_CursorShapeFull');

    type
        TKey = CCardinal;

        TCursorShape = (CursorIgnored, CursorHidden, CursorNormal,
        CursorFat, CursorBlock);

        TCRTUpdate = (UpdateNever, UpdateWaitInput, UpdateInput,
                      UpdateRegularly, UpdateAlways);

        TPoint = record
            x, y: CInteger
        end;

        PCharAttr = ^TCharAttr;
        TCharAttr = record
            ch       : Char;
            Attr     : TTextAttr;
            PCCharSet: Boolean
        end;

        PCharAttrs = ^TCharAttrs;
        TCharAttrs = array [1 .. MaxVarSize div SizeOf (TCharAttr)] of
            TCharAttr;

        TWindowXYInternalCard8 = Cardinal attribute (Size = 8);
        TWindowXYInternalFill = Integer attribute (Size = BitSizeOf
            (CCardinal) - 16);
        TWindowXY = packed record
            {$ifdef __BYTES_BIG_ENDIAN__}
            Fill: TWindowXYInternalFill;
            y, x: TWindowXYInternalCard8
            {$elif defined (__BYTES_LITTLE_ENDIAN__)}
            x, y: TWindowXYInternalCard8;
            Fill: TWindowXYInternalFill
            {$else}
            {$error Endianness is not defined!}
            {$endif}
        end;

    { Make sure TWindowXY really has the same size as WindMin and
    WindMax. The value of the constant will always be True, and is of
    no further interest. }
    const
        AssertTWindowXYSize = CompilerAssert ((SizeOf (TWindowXY) = SizeOf
        (WindMin)) and (SizeOf (TWindowXY) = SizeOf (WindMax)));

    var
        { Window upper and left coordinates. More comfortable to access
        than WindMin, but also *obsolete*. WindMin and WindowMin still
        work, but have the problem that they implicitly limit the window
        size to 255x255 characters. Though that's not really small for a
        text window, it's easily possible to create bigger ones (e.g. in
        an xterm with a small font, on a high resolution screen and/or
        extending over several virutal desktops). When using coordinates
        greater than 254, the corresponding bytes in WindowMin/WindowMax
        will be set to 254, so, e.g., programs which do
        Inc (WindowMin.x) will not fail quite as badly (but probably
        still fail). The routines Window and GetWindow use Integer
        coordinates, and don't suffer from any of these problems, so
        they should be used instead. }
        WindowMin: TWindowXY absolute WindMin;

        { Window lower right coordinates. More comfortable to access than
        WindMax, but also *obsolete* (see the comments for WindowMin).
        Use Window and GetWindow instead. }
        WindowMax: TWindowXY absolute WindMax;

        { The attribute set by NormVideo }
        NormAttr: TTextAttr = 7; attribute (name = 'crt_NormAttr');

        { Tells whether the current mode is monochrome }
        IsMonochrome: Boolean = False; attribute (name
        = 'crt_IsMonochrome');

        { This value can be set to a combination of the shFoo constants
        and will be ORed to the actual shift state returned by
        GetShiftState. This can be used to easily simulate shift keys on
        systems where they can't be accessed. }
        VirtualShiftState: CInteger = 0; attribute (name
        = 'crt_VirtualShiftState');
    '''

    '''
        Returns the size of the screen. Note: In BP's WinCRT unit,
        ScreenSize is a variable. But since writing to it from a program
        is pointless, anyway, providing a def here should not cause
        any incompatibility.

        @return TPoint
    '''
    def ScreenSize():
        pass

    '''
        Change the screen size if possible.
        @param int x
        @param int y
    '''
    def SetScreenSize(x, y):
        pass

    '''
        Turns colors off or on.
        @param boolean Monochrome
    '''
    def SetMonochrome(Monochrome):
        pass

    '''
        Get the extent of the current window. Use this def rather
        than reading WindMin and WindMax or WindowMin and WindowMax, since
        this routine allows for window sizes larger than 255. The
        resulting coordinates are 1-based (like in Window, unlike WindMin,
        WindMax, WindowMin and WindowMax). Any of the parameters may be
        Null in case you're interested in only some of the coordinates.
    '''
    # def GetWindow (var x1, y1, x2, y2: Integer); attribute (name
    #   = 'crt_GetWindow');

    '''
        Determine when to update the screen. The possible values are the
        following. The given conditions *guarantee* updates. However,
        updates may occur more frequently (even if the update level is set
        to UpdateNever). About the default value, see the comments for
        CRTInit.

        UpdateNever    : never (unless explicitly requested with
                         CRTUpdate)
        UpdateWaitInput: before Delay and CRT input, unless typeahead is
                         detected
        UpdateInput    : before Delay and CRT input
        UpdateRegularly: before Delay and CRT input and otherwise in
                         regular intervals without causing too much
                         refresh. This uses a timer on some systems
                         (currently, Unix with ncurses). This was created
                         for BP compatibility, but for many applications,
                         a lower value causes less flickering in the
                         output, and additionally, timer signals won't
                         disturb other operations. Under DJGPP, this
                         always updates immediately, but this fact should
                         not mislead DJGPP users into thinking this is
                         always so.
        UpdateAlways   : after each output. This can be very slow. (Not so
                         under DJGPP, but this fact should not mislead
                         DJGPP users ...)
    '''
    # def SetCRTUpdate (UpdateLevel: TCRTUpdate); external
    # name 'crt_SetUpdateLevel';

    '''
        Do an update now, independently of the update level
    '''
    # def CRTUpdate; external name 'crt_Update';

    '''
        Do an update now and completely redraw the screen
    '''
    # def CRTRedraw; external name 'crt_Redraw';

    '''
        Produce a beep or a screen flash
    '''
    # def Flash; external name 'crt_Flash';

    '''
        Get size of current window (calculated using GetWindow)
    '''
    # def  GetXMax: Integer;
    # def  GetYMax: Integer;

    '''
        Get/goto an absolute position
    '''
    # def  WhereXAbs: Integer;
    # def  WhereYAbs: Integer;
    # def GotoXYAbs (x, y: Integer);

    '''
        Turn scrolling on or off
    '''
    # def SetScroll (State: Boolean); external name 'crt_SetScroll';

    '''
        Read back whether scrolling is enabled
    '''
    # def  GetScroll: Boolean; external name 'crt_GetScroll';

    '''
        Determine whether to interpret non-ASCII characters as PC ROM
        characters (True), or in a system dependent way (False). About the
        default, see the comments for CRTInit.
    '''
    # def SetPCCharSet (PCCharSet: Boolean); external
    #   name 'crt_SetPCCharSet';

    '''
        Read back the value set by SetPCCharSet
    '''
    # def GetPCCharSet: Boolean; external name 'crt_GetPCCharSet';

    '''
        Determine whether to interpret #7, #8, #10, #13 as control
        characters (True, default), or as graphics characters (False)
    '''
    # def SetControlChars (UseControlChars: Boolean); external
    #   name 'crt_SetControlChars';

    '''
        Read back the value set by SetControlChars
    '''
    # def  GetControlChars: Boolean; external
    #   name 'crt_GetControlChars';

    # def SetCursorShape (Shape: TCursorShape); external
    #   name 'crt_SetCursorShape';
    # def  GetCursorShape: TCursorShape; external
    #   name 'crt_GetCursorShape';

    # def HideCursor;
    # def HiddenCursor;
    # def NormalCursor;
    # def FatCursor;
    # def BlockCursor;
    # def IgnoreCursor;

    '''
        Simulates a block cursor by writing a block character onto the
        cursor position. The def automatically finds the topmost
        visible panel whose shape is not CursorIgnored and places the
        simulated cursor there (just like the hardware cursor), with
        matching attributes, if the cursor shape is CursorFat or
        CursorBlock (otherwise, no simulated cursor is shown).

        Calling this def again makes the simulated cursor disappear.
        In particular, to get the effect of a blinking cursor, you have to
        call the def repeatedly (say, 8 times a second). CRT will
        not do this for you, since it does not intend to be your main
        event loop.
    '''
    # def SimulateBlockCursor; external
    #    name 'crt_SimulateBlockCursor';

    '''
        Makes the cursor simulated by SimulateBlockCursor disappear if it
        is active. Does nothing otherwise. You should call this procedure
        after using SimulateBlockCursor before doing any further CRT
        output (though failing to do so should not hurt except for
        possibly leaving the simulated cursor in its old position longer
        than it should).
    '''
    # def SimulateBlockCursorOff; external
    #    name 'crt_SimulateBlockCursorOff';

    # def  GetTextColor: Integer;
    # def  GetTextBackground: Integer;

    '''
        Write string at the given position without moving the cursor.
        Truncated at the right margin.
    '''
    # def WriteStrAt (x, y: Integer; const s: String; Attr:
    #    TTextAttr);

    '''
        Write (several copies of) a char at then given position without
        moving the cursor. Truncated at the right margin.
    '''
    # def WriteCharAt (x, y, Count: Integer; ch: Char; Attr:
    #    TTextAttr);

    '''
        Write characters with specified attributes at the given position
        without moving the cursor. Truncated at the right margin.
    '''
    # def WriteCharAttrAt (x, y, Count: CInteger; CharAttr:
    #     PCharAttrs); external name 'crt_WriteCharAttrAt';

    '''
        Write a char while moving the cursor
        def WriteChar (ch: Char);
    '''

    '''
        Read a character from a screen position
    '''
    # def ReadChar (x, y: CInteger; var ch: Char; var Attr:
    #    TTextAttr); external name 'crt_ReadChar';

    '''
        Change only text attributes, leave characters. Truncated at the
        right margin.
    '''
    # def ChangeTextAttr (x, y, Count: Integer; NewAttr: TTextAttr);

    '''
        Fill current window
    '''
    # def FillWin(ch: Char; Attr: TTextAttr); external
    #    name 'crt_FillWin';

    '''
        Calculate size of memory required for ReadWin in current window.
    '''
    # def WinSize: SizeType; external name 'crt_WinSize';

    '''
        Save window contents. Buf must be WinSize bytes large.
    '''
    # def ReadWin (var Buf); external name 'crt_ReadWin';

    '''
        Restore window contents saved by ReadWin. The size of the current
        window must match the size of the window from which ReadWin was
        used, but the position may be different.
    '''
    # def WriteWin (const Buf); external name 'crt_WriteWin';

    '''
    type
        WinState = record
            x1, y1, x2, y2, WhereX, WhereY, NewX1, NewY1, NewX2, NewY2: Integer
            TextAttr: TTextAttr;
            CursorShape: TCursorShape;
            ScreenSize: TPoint;
            Buffer: ^Byte
        end;
    '''

    '''
        Save window position and size, cursor position, text attribute and
        cursor shape -- *not* the window contents.
    '''
    # def SaveWin (var State: WinState)

    '''
        Make a new window (like Window), and save the contents of the
        screen below the window as well as the position and size, cursor
        position, text attribute and cursor shape of the old window.
    '''
    # def MakeWin (var State: WinState; x1, y1, x2, y2: Integer);

    '''
        Create window in full size, save previous text mode and all values
        that MakeWin does.
    '''
    # def SaveScreen (var State: WinState);

    '''
        Restore the data saved by SaveWin, MakeWin or SaveScreen.
    '''
    # def RestoreWin (var State: WinState);


    ''' TPCRT compatibility '''

    '''
        Write a string at the given position without moving the cursor.
        Truncated at the right margin.
    '''
    # def WriteString (const s: String; y, x: Integer);

    '''
        Write a string at the given position with the given attribute
        without moving the cursor. Truncated at the right margin.
    '''
    # def FastWriteWindow (const s: String; y, x: Integer; Attr:
    #   TTextAttr);

    '''
        Write a string at the given absolute position with the given
        attribute without moving the cursor. Truncated at the right
        margin.
    '''
    # def FastWrite       (const s: String; y, x: Integer; Attr:
    #   TTextAttr);

    '''
        0-based coordinates!
    '''
    # def CursorTo (x, y: Integer); attribute (name
    #   = 'crt_CursorTo');

    '''
        Dummy
    '''
    # def ScrollTo (x, y: Integer); attribute (name
    #   = 'crt_ScrollTo');

    '''
        Dummy
    '''
    # def TrackCursor; attribute (name = 'crt_TrackCursor');



