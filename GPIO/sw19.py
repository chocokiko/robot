#!/usr/bin/env python
#Simply switches Pin 11 off (High)

# Must be run as root - sudo python blink11.py 

import time, RPi.GPIO as GPIO

GPIO.setwarnings(False)
GPIO.setmode(GPIO.BOARD)
GPIO.setup(11, GPIO.OUT)
GPIO.setup(19, GPIO.IN, pull_up_down=GPIO.PUD_UP)

while True:
  if GPIO.input(19):
    GPIO.output(11, 1)
  else:
    GPIO.output(11, 0)
