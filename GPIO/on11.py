#!/usr/bin/env python
#Simply switches Pin 11 on (Low)

# Must be run as root - sudo python blink11.py 

import time, RPi.GPIO as GPIO

GPIO.setwarnings(False)
GPIO.setmode(GPIO.BOARD)
GPIO.setup(11, GPIO.OUT)

LEDon = GPIO.output(11, 0)
