#!/usr/bin/env python
#Simply switches Pin 11 off (High)

# Must be run as root - sudo python blink11.py 

import time, RPi.GPIO as GPIO

GPIO.setwarnings(False)
GPIO.setmode(GPIO.BOARD)

SWITCH1_PIN = 19
SWITCH2_PIN = 21
OUTPUTS = {'led1':7,
           'led2':11,
           'led3':12,
           'led4':13,
           'led5':15,
           'led6':16,
           'led7':18,
           'led8':22,
           'led9':24,
           'led10':26,
           'led11':8,
           'led12':10}
LEDS = ['led1','led2','led3','led4','led5','led6','led7','led8','led9','led10','led11','led12']

LEDOFF = 1
LEDON = 0
NUMLEDS = 12

for idx in OUTPUTS.keys():
    GPIO.setup(OUTPUTS[idx],GPIO.OUT)

GPIO.setup(SWITCH1_PIN,GPIO.IN,pull_up_down=GPIO.PUD_UP)
GPIO.setup(SWITCH2_PIN,GPIO.IN,pull_up_down=GPIO.PUD_UP)

def startupdance():
    for val in LEDS:
        GPIO.output(OUTPUTS[val],LEDON)
        time.sleep(0.1)
    for val in reversed(LEDS):
        GPIO.output(OUTPUTS[val],LEDOFF)
        time.sleep(0.1)

def alloff ():
    for val in LEDS:
        GPIO.output(OUTPUTS[val],LEDOFF)

def allon ():
    for val in LEDS:
        GPIO.output(OUTPUTS[val],LEDON)

def chase1 ():
    for val in LEDS:
        GPIO.output(OUTPUTS[val],LEDON);
        time.sleep(0.5)
        GPIO.output(OUTPUTS[val],LEDOFF);

def chase2 ():
    a = 0
    while a < NUMLEDS:
        GPIO.output(OUTPUTS[LEDS[a]],LEDON);
        time.sleep(0.5)
        a = a + 1
        GPIO.output(OUTPUTS[LEDS[a-1]],LEDOFF);

#startupdance()
#alloff()
chase2()


