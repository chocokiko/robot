#!/usr/bin/env python
#Simply switches Pin 11 off (High)

# Must be run as root - sudo python blink11.py 

import time, RPi.GPIO as GPIO

GPIO.setwarnings(False)
GPIO.setmode(GPIO.BOARD)
GPIO.setup(7, GPIO.OUT)
GPIO.setup(11, GPIO.OUT)
GPIO.setup(12, GPIO.OUT)
GPIO.setup(13, GPIO.OUT)
GPIO.setup(15, GPIO.OUT)
GPIO.setup(16, GPIO.OUT)
GPIO.setup(18, GPIO.OUT)
GPIO.setup(22, GPIO.OUT)
GPIO.setup(24, GPIO.OUT)
GPIO.setup(26, GPIO.OUT)
GPIO.setup(8, GPIO.OUT)
GPIO.setup(10, GPIO.OUT)

GPIO.output(7, 1)
GPIO.output(11, 1)
GPIO.output(12, 1)
GPIO.output(13, 1)
GPIO.output(15, 1)
GPIO.output(16, 1)
GPIO.output(18, 1)
GPIO.output(22, 1)
GPIO.output(24, 1)
GPIO.output(26, 1)
GPIO.output(8, 1)
GPIO.output(10, 1)
