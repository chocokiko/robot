#!/usr/bin/env python
#Simply tries to swithc Pin 11 on for 1 sec and off for 2 secs
#V0.2 26Aug12

# Must be run as root - sudo python blink11.py 

import time, RPi.GPIO as GPIO

GPIO.setmode(GPIO.BOARD)
GPIO.setup(11, GPIO.OUT)

OUTPUTS = {'led1l':7,
           'led2':11,
           'led3':12,
           'led4':13,
           'led5':15,
           'led6':16,
           'led7':18,
           'led8':22,
           'led9':24,
           'led10':26,
           'led11':8,
           'led12':10}

for idx in OUTPUTS.keys():
    GPIO.setup(OUTPUTS[idx], GPIO.OUT)
    GPIO.output(OUTPUTS[idx], 0)

while True:
	LEDon = GPIO.output(11, 0)
	time.sleep(0.2)
	LEDoff = GPIO.output(11, 1)
	time.sleep(0.2)

