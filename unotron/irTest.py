import sys, time
import unotron

unotron.init()



try:
    lastLineL = unotron.irLeftLine()
    lastLineR = unotron.irRightLine()
    print 'LeftLine, RightLine:', lastLineL, lastLineR
    print
    while True:
        newLineL = unotron.irLeftLine()
        newLineR = unotron.irRightLine()
        if (newLineL != lastLineL) or (newLineR != lastLineR):
            print 'LeftLine, RightLine:', newLineL, newLineR
            print
            lastLineL = newLineL
            lastLineR = newLineR
        time.sleep(0.1)
                          
except KeyboardInterrupt:
    print

finally:
    unotron.cleanup()
