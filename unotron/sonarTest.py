import time
import unotron

unotron.init()

try:
    while True:
        dist = unotron.getDistance()
        print "Distance: ", int(dist)
        time.sleep(1)

except KeyboardInterrupt:
    print
    pass

finally:
    unotron.cleanup()
